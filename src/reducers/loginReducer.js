import * as types from '../actions'

let initialState = { response: {} }

export default function loginReducer(state = initialState, action) {
    const response = action.response
    switch (action.type) {
        case types.LOGIN_USER_SUCCESS:
            return { ...state, response: response }
        case types.LOGIN_USER_ERROR:
            return { ...state, response: response }
        default:
            return state
    }
}
