import firebase from 'firebase'

export const loginUserService = ({ email, password }) => {
    return firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then((response) => {
            alert(
                `User with email ${response.user.email} successfully logged in!`
            )
            return {
                response: response.user.providerData,
                status: 'OK',
            }
        })
        .catch((error) => {
            alert('Something went wrong!')
            return {
                response: error,
                status: 'ERROR',
            }
        })
}

export const registerUserService = ({ email, password }) => {
    return firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then((response) => {
            alert(
                `User with email ${response.user.email} successfully registered!`
            )
            return {
                response: response.user.providerData,
                status: 'OK',
            }
        })
        .catch((error) => {
            alert('Something went wrong!')
            return {
                response: error,
                status: 'ERROR',
            }
        })
}
